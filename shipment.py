# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError


def set_depends(field_names, instance, Model):
    pool = Pool()

    for fname in field_names:
        if not hasattr(instance, fname) and hasattr(Model, fname):
            default_method = getattr(Model, 'default_%s' % fname, None)
            default_value = default_method() if default_method else None
            field = getattr(Model, fname)
            if default_value and isinstance(field, fields.Many2One):
                TargetModel = pool.get(field.model_name)
                default_value = TargetModel(default_value)
            setattr(instance, fname, default_value)


class CreatePurchaseMixin(object):
    _purchase_moves = None

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'delete_purchase': {
                'invisible': ~Eval('state').in_(['received', 'done']),
                'icon': 'tryton-back',
                'depends': ['state']
            }
        })
        cls._transitions.add(('done', 'cancelled'))

    @classmethod
    def copy(cls, shipments, default=None):
        pool = Pool()
        Move = pool.get('stock.move')

        if default is None:
            default = {}
        default = default.copy()
        default['moves'] = None
        copies = super().copy(shipments, default=default)
        for shipment, copy in zip(shipments, copies):
            Move.copy(getattr(shipment, cls._purchase_moves, []), default={
                'shipment': str(copy),
                'origin': None,
            })
        return copies

    def create_purchase(self, warehouse=None):
        pool = Pool()
        Warning = pool.get('res.user.warning')

        product2moves = {}
        product2quantity = {}
        moves_fname = self._purchase_moves
        for move in getattr(self, moves_fname, []):
            if move.origin:
                continue
            if not move.product.purchasable:
                raise UserError(gettext(
                    'purchase_from_shipment.'
                    'msg_stock_shipment_purchasable_product',
                    product=move.product.rec_name))
            key = (move.product, move.uom, move.unit_price)
            product2moves.setdefault(key, []).append(move)
            product2quantity.setdefault(key, 0.0)
            product2quantity[key] += move.quantity

        if not product2quantity:
            return

        if self.must_warn_create_purchase():
            key = 'create_purchase_from_move'
            if Warning.check(key):
                raise UserWarning(key, gettext(
                    'purchase_from_shipment.'
                    'msg_stock_shipment_create_purchase_from_move',
                    model_name=self.error_model_name(),
                    shipment=self.rec_name,
                    products_wo_origin=', '.join([p.rec_name
                            for p, uom, price in list(product2moves.keys())])))

        sign = -1.0 if self.__name__ == 'stock.shipment.in.return' else 1.0

        purchase = self.get_purchase(warehouse)
        lines = list(purchase.lines) or []
        for product_key, moves in product2moves.items():
            product, uom, price = product_key
            values = {
                'purchase': purchase,
                'type': 'line',
                'product': product,
                'quantity': product2quantity[product_key] * sign,
                'unit_price': price,
                'unit': uom
            }
            purchase_line = self.get_purchase_line(**values)
            if purchase_line:
                purchase_line.moves = moves
                lines.append(purchase_line)
        purchase.lines = lines
        purchase.save()
        return purchase

    def must_warn_create_purchase(self):
        return True

    def get_purchase(self, warehouse):
        pool = Pool()
        Date = pool.get('ir.date')
        Purchase = pool.get('purchase.purchase')

        # TODO: search existing purchase?
        purchase = Purchase()
        purchase.invoice_method = 'shipment'
        purchase.company = self.company
        purchase.purchase_date = self.effective_date or Date.today()
        purchase.party = self.supplier
        purchase.reference = self.reference
        purchase.payment_term = None
        purchase.lines = []
        set_depends(Purchase.party.on_change, purchase, Purchase)
        purchase.on_change_party()
        purchase.warehouse = warehouse

        return purchase

    def get_purchase_line(self, **kwargs):
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')

        line = PurchaseLine(**kwargs)
        line.on_change_product()
        line.unit = kwargs.get('unit', line.unit)
        line.unit_price = kwargs.get('unit_price', line.unit_price)
        return line

    @classmethod
    def delete_purchase(cls, records):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        Purchase = pool.get('purchase.purchase')
        PurchaseLine = pool.get('purchase.line')
        Move = pool.get('stock.move')

        purchases = []
        invoice_lines = []
        for record in records:
            moves = [m for m in getattr(record, cls._purchase_moves, [])
                if isinstance(m.origin, PurchaseLine)]
            all_purchases = set([m.purchase for m in moves])
            model_name = record.error_model_name()
            if not all_purchases or len(all_purchases) > 1:
                raise UserError(gettext(
                    'purchase_from_shipment.'
                    'msg_stock_shipment_delete_purchase_shipments',
                    model_name=model_name,
                    shipment=record.rec_name))
            Move.write(moves, {'origin': None})
            purchase, = list(all_purchases)
            if any(l.invoice_lines for l in purchase.lines):
                if (purchase.invoice_state not in ('none', 'waiting')
                        or len(purchase.invoices) > 1
                        or (purchase.invoices
                            and purchase.invoices[0].state not in (
                                'draft', 'cancelled')
                        )):
                    raise UserError(gettext(
                        'purchase_from_shipment.'
                        'msg_stock_shipment_delete_purchase_invoice',
                        model_name=model_name,
                        shipment=record.rec_name))
                invoice_lines.extend(list(
                    set(i for l in purchase.lines for i in l.invoice_lines)))
            if len(purchase.shipments) > 1:
                raise UserError(gettext(
                    'purchase_from_shipment.'
                    'msg_stock_shipment_delete_purchase_shipments',
                    model_name=model_name,
                    shipment=record.rec_name))
            purchases.append(purchase)

        # consider invoice grouping
        if invoice_lines:
            InvoiceLine.write(invoice_lines, {'origin': None})

        if purchases:
            Purchase.cancel(purchases)
            Purchase.delete(purchases)

        if invoice_lines:
            invoices = set([l.invoice for l in invoice_lines if l.invoice])
            InvoiceLine.delete(invoice_lines)

            if invoices:
                lines = InvoiceLine.search([
                    ('invoice', 'in', list(map(int, invoices))),
                    ('type', '=', 'line')])
                if not lines:
                    Invoice.delete(invoices)

        with Transaction().set_context(check_origin=False,
                check_shipment=False):
            cls.cancel(records)
            cls.draft(records)

    def error_model_name(self):
        pool = Pool()
        IrModel = pool.get('ir.model')

        model, = IrModel.search([
                    ('model', '=', self.__name__),
                ])
        return model.name


class ShipmentIn(CreatePurchaseMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'
    _purchase_moves = 'incoming_moves'

    @classmethod
    def receive(cls, shipments):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')

        purchases = []
        for shipment in shipments:
            purchase = shipment.create_purchase(warehouse=shipment.warehouse)
            if purchase:
                purchases.append(purchase)
        if purchases:
            Purchase.quote(purchases)
            Purchase.confirm(purchases)
            Purchase.process(purchases)
        super(ShipmentIn, cls).receive(shipments)


class ShipmentInReturn(CreatePurchaseMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'
    _purchase_moves = 'moves'

    @classmethod
    def assign_try(cls, shipments):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')

        purchases = []
        for shipment in shipments:
            purchase = shipment.create_purchase()
            if purchase:
                purchases.append(purchase)
        if purchases:
            Purchase.quote(purchases)
            Purchase.confirm(purchases)
            Purchase.process(purchases)
        return super().assign_try(shipments)


class ReturnShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return_shipment'

    def _get_return_shipment(self, shipment_in):
        shipment = super()._get_return_shipment(
            shipment_in)
        shipment.supplier = shipment_in.supplier
        return shipment


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._transitions.add(
            ('processing', 'cancelled'))

    def _get_return_shipment(self):
        shipment = super()._get_return_shipment()
        shipment.supplier = self.party
        return shipment
